// +build arm

package SlowControl

import (
	"syscall"
)

func PreciserSleep(nanoseconds int) {
	wait := syscall.Timespec{Nsec: int32(nanoseconds)}
	leftover := syscall.Timespec{}
	syscall.Nanosleep(&wait, &leftover)

}
