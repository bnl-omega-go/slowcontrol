//go:build linux || darwin
// +build linux darwin

package SlowControl

import (
	"testing"
)

func TestFromTwoComplement(t *testing.T) {
	type args struct {
		value  int
		length int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "1", args: args{0b11111111, 8}, want: -1},
		{name: "2", args: args{0b11111110, 8}, want: -2},
		{name: "3", args: args{0b10000000, 8}, want: -128},
		{name: "4", args: args{0b01111111, 8}, want: 127},
		{name: "4", args: args{0b1001001001001001, 16}, want: -28087},
		{name: "4", args: args{0b010010010010010011111000, 24}, want: 4793592},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromTwoComplement(tt.args.value, tt.args.length); got != tt.want {
				t.Errorf("TwoComplement() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToTwoComplement(t *testing.T) {
	type args struct {
		value  int
		length int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "-126", args: args{-126, 8}, want: 0b10000010},
		{name: "127", args: args{127, 8}, want: 0b01111111},
		{name: "-28087", args: args{-28087, 16}, want: 0b1001001001001001},
		{name: "127", args: args{4793592, 24}, want: 0b010010010010010011111000},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ToTwoComplement(tt.args.value, tt.args.length); got != tt.want {
				t.Errorf("ToTwoComplement() = %v, want %v", got, tt.want)
			}
		})
	}
}
