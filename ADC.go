//go:build linux || darwin
// +build linux darwin

package SlowControl

import (
	"io/ioutil"
	"log"
	"strings"
)

type ADCController struct {
	Registers ADCRegisterMap
	Spictrl   *SPIController
}

func NewADCController(addr, mode int) *ADCController {
	Spictrl := SPIController{}
	Spictrl.Init()
	Spictrl.AddDevice("ADC", addr, mode)
	ctrl := ADCController{Spictrl: &Spictrl}
	ctrl.Registers.Init()
	return &ctrl
}

func (ctrl *ADCController) SetPage(page string) {
	var pgsel []byte

	switch page {
	case "ADS52J65_GLOBAL":
		pgsel = []byte{0x0, 0x0, 0x0}
	case "ADS52J65_MISC":
		//page selection to Misc
		pgsel = []byte{0x12, 0x0, 0x1}
	case "ADS52J65_ADC_CH":
		//page selection to ADC
		pgsel = []byte{0x11, 0x0, 0xFF}
	case "ADS52J65_DIG_CH":
		//page selection to 8CH
		pgsel = []byte{0x12, 0x0, 0b10}
	case "ADS52J65_Common_DIG":
		//page selection to DIG
		pgsel = []byte{0x13, 0x0, 0xFF}
	case "ADS52J65_JESD1":
		//page selection to 8CH
		pgsel = []byte{0x12, 0x0, 0b10}
	case "ADS52J65_JESD2":
		//page selection to 8CH
		pgsel = []byte{0x12, 0x0, 0b10}
	default:
		return
	}

	var err2 error
	if page == "ADS52J65_JESD1" {
		_, err2 = ctrl.Spictrl.Transfer("ADC", []byte{0x1E, 0x0, 0b10})
	}
	if page == "ADS52J65_JESD2" {
		_, err2 = ctrl.Spictrl.Transfer("ADC", []byte{0x1E, 0x0, 0b0})
	}

	_, err := ctrl.Spictrl.Transfer("ADC", pgsel)

	if err != nil || err2 != nil {
		log.Fatal(err, err2)
	}
}

func ReadADCFile(filename string) [][]string {
	contentbytes, err := ioutil.ReadFile(filename)
	content := strings.Split(string(contentbytes), "\n")
	if err != nil {
		log.Fatal(err)
	}
	var result [][]string
	for _, v := range content {
		if len(v) > 0 {
			prewords := strings.Fields(string(v))
			words := strings.Split(prewords[0], "|")
			words = append(words, prewords[1])
			//log.Println(words)
			result = append(result, words)
		}
	}
	return result
}

func (ctrl *ADCController) ConfigureADCFromFile(filename string) {

	data := ReadADCFile(filename)
	for _, reg := range data {

		page := reg[0]
		address, _ := parsehex(reg[1])
		value, _ := parsehex(reg[2])

		//Write
		ctrl.SetPage(page)
		data := []byte{byte(address), byte((value >> 8) & 0xFF), byte(value & 0xFF)}
		log.Printf("Writing to page %v, addr: 0x%x, value : 0x%x", page, address, value)
		_, err := ctrl.Spictrl.Transfer("ADC", data)
		if err != nil {
			log.Println("WARNING ", err)
		}

		//Readback
		setread := []byte{0x0, 0x0, 0x2}
		_, err = ctrl.Spictrl.Transfer("ADC", setread)
		readback, err2 := ctrl.Spictrl.Transfer("ADC", data)
		setread[2] = 0x0
		_, err3 := ctrl.Spictrl.Transfer("ADC", setread)

		if (err != nil) || (err2 != nil) || (err3 != nil) {
			log.Printf("ERROR SetR : %v , RB: %v, SETW : %v ", err, err2, err3)
		}

		//Assemble and check readback value
		read_value := (uint16(readback[1]) << 8) + uint16(readback[2])
		if read_value == value {
			log.Printf("Register value (exp. vs read) 0x%x == 0x%x", value, read_value)
		} else {
			log.Printf("Register value (exp. vs read) 0x%x != 0x%x", value, read_value)
		}
	}
}

func (ctrl *ADCController) TransferRegister(name string, value uint16) []byte {
	page, regaddr, value := ctrl.Registers.SetRegister(name, value)
	err := []error{nil, nil}
	var readback []byte
	pgsel := make([]byte, 2)
	//Register write data

	switch page {
	case "MASTER":
		pgsel = []byte{0x0, 0x0, 0x0}
	case "MISC":
		//page selection to Misc
		pgsel = []byte{0x12, 0x0, 0x1}
	case "ADC":
		//page selection to ADC
		pgsel = []byte{0x11, 0x0, 0xFF}
	case "8CH":
		//page selection to 8CH
		pgsel = []byte{0x12, 0x0, 0b10}
	case "DIG":
		//page selection to DIG
		pgsel = []byte{0x13, 0x0, 0xFF}
	}

	_, err[0] = ctrl.Spictrl.Transfer("ADC", pgsel)

	data := []byte{byte(regaddr), byte((value >> 8) & 0xFF), byte(value & 0xFF)}
	readback, err[1] = ctrl.Spictrl.Transfer("ADC", data)

	for _, e := range err {
		if e != nil {
			log.Fatal(err)
		}
	}
	return readback
}

type ADCRegister struct {
	Name                string
	length              int64
	shift               int64
	Value, defaultvalue uint16
	read, write         bool
}

func FormRegister(logicalregs []ADCRegister) uint16 {
	value := 0

	//TODO mask v.Value properly
	for _, v := range logicalregs {
		value = value + int(v.Value<<uint16(v.shift))
	}
	return uint16(value)
}

func (ctrl *ADCController) Reset() {

	resetwrd := [][]byte{
		{0x0, 0x0, 0x1},
		{0x0, 0x0, 0x0},
		{0x11, 0x0, 0xFF},
		{0x25, 0x0, 0x2},
		{0x11, 0x0, 0x0},
	}

	buf := make([]byte, 3)
	for _, wrd := range resetwrd {
		buf = wrd
		ctrl.Spictrl.Transfer("ADC", buf)
	}
}
