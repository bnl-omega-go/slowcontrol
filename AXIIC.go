//go:build linux || darwin
// +build linux darwin

package SlowControl

import (
	//"periph.io/x/conn/v3/driver/driverreg"

	"fmt"
	"log"

	AXI "gitlab.cern.ch/bnl-omega-go/axi"
	//"periph.io/x/conn/v3/i2c/i2creg"
	//"periph.io/x/host/v3"
)

func NewAXIIC(base int64, delay int) *AXIIC {
	// ALFE2 I2C Custom control registers
	// ALFE_I2C_ADDR  : 0x0020
	// ALFE_I2C_WRITE : 0x0028
	// ALFE_I2C_RW    : 0x002c
	// ALFE_I2C_START : 0x0030
	// ALFE_I2C_READ  : 0x0024
	// ALFE_I2C_STOP  : 0x0034
	// ALFE_I2C_ERR   : 0x0038
	// ALFE_I2C_CLOCK   : 0x003C
	// ALFE_I2C_RSTB   : 0x0040

	registers := map[string][]int64{"ALFE_I2C_ADDR": []int64{0, 0x0020, int64(AXI.AXIRW)},
		"ALFE_I2C_WRITE": []int64{base, 0x0028, int64(AXI.AXIRW)}, "ALFE_I2C_RW": []int64{base, 0x002c, int64(AXI.AXIRW)},
		"ALFE_I2C_START": []int64{base, 0x0030, int64(AXI.AXIRW)}, "ALFE_I2C_READ": []int64{base, 0x0024, int64(AXI.AXIR)},
		"ALFE_I2C_STOP": []int64{base, 0x0034, int64(AXI.AXIRW)}, "ALFE_I2C_ERR": []int64{base, 0x0038, int64(AXI.AXIR)},
		"ALFE_I2C_CLOCK": []int64{base, 0x003c, int64(AXI.AXIRW)}, "ALFE_I2C_RSTB": []int64{base, 0x0040, int64(AXI.AXIR)},
	}

	pages := map[string]AXI.Page{"AXI": AXI.Page{Base: base, Length: 4096}}

	axiic := AXIIC{ctrl: &AXI.AXIController{}, delay: delay}

	(*axiic.ctrl).BookPages(pages)
	//ctrl.ctrl.BookRegisters(registers)
	for key, reg := range registers {
		(*axiic.ctrl).BookRegister("AXI", key, int(reg[1])/4, int(reg[2]))
	}

	return &axiic
}

func NewConnectedIIC(axictrl *AXI.AXIController, base int64, delay int) *AXIIC {
	// ALFE2 I2C Custom control registers
	// ALFE_I2C_ADDR  : 0x0020
	// ALFE_I2C_WRITE : 0x0028
	// ALFE_I2C_RW    : 0x002c
	// ALFE_I2C_START : 0x0030
	// ALFE_I2C_READ  : 0x0024
	// ALFE_I2C_STOP  : 0x0034
	// ALFE_I2C_ERR   : 0x0038
	// ALFE_I2C_CLOCK   : 0x003C
	// ALFE_I2C_RSTB   : 0x0040

	registers := map[string][]int64{"ALFE_I2C_ADDR": []int64{0, 0x0020, int64(AXI.AXIRW)},
		"ALFE_I2C_WRITE": []int64{base, 0x0028, int64(AXI.AXIRW)}, "ALFE_I2C_RW": []int64{base, 0x002c, int64(AXI.AXIRW)},
		"ALFE_I2C_START": []int64{base, 0x0030, int64(AXI.AXIRW)}, "ALFE_I2C_READ": []int64{base, 0x0024, int64(AXI.AXIR)},
		"ALFE_I2C_STOP": []int64{base, 0x0034, int64(AXI.AXIRW)}, "ALFE_I2C_ERR": []int64{base, 0x0038, int64(AXI.AXIR)},
		"ALFE_I2C_CLOCK": []int64{base, 0x003c, int64(AXI.AXIRW)}, "ALFE_I2C_RSTB": []int64{base, 0x0040, int64(AXI.AXIR)},
	}

	pages := map[string]AXI.Page{"AXI": AXI.Page{Base: base, Length: 4096}}

	axiic := AXIIC{ctrl: axictrl, delay: delay}

	(*axiic.ctrl).BookPages(pages)
	//ctrl.ctrl.BookRegisters(registers)
	for key, reg := range registers {
		(*axiic.ctrl).BookRegister("AXI", key, int(reg[1])/4, int(reg[2]))
	}

	return &axiic
}

type AXIIC struct {
	ctrl  *AXI.AXIController
	delay int
}

// func (a *AXIIC) String() string {
// 	return "AXI"
// }

// func (a *AXIIC) SetSpeed(f physic.Frequency) error {
// 	return nil
// }

func (a *AXIIC) Reset() {
	(*a.ctrl).Write("ALFE_I2C_RSTB", 1)
	(*a.ctrl).Write("ALFE_I2C_RSTB", 0)
	(*a.ctrl).Write("ALFE_I2C_RSTB", 1)

}

func (a *AXIIC) SetClockDivider(div uint32) {
	if div < 256 {
		(*a.ctrl).Write("ALFE_I2C_CLOCK", div)
	} else {
		log.Printf("Clock divider must be < 256")
	}
}

func (ctrl *AXIIC) Write(address, value uint32) error {

	(*ctrl.ctrl).Write("ALFE_I2C_RW", 0)
	(*ctrl.ctrl).Write("ALFE_I2C_ADDR", uint32(address))
	(*ctrl.ctrl).Write("ALFE_I2C_WRITE", uint32(value))
	(*ctrl.ctrl).Write("ALFE_I2C_START", 0)
	//time.Sleep(10 * time.Microsecond)
	PreciserSleep(10000)
	(*ctrl.ctrl).Write("ALFE_I2C_START", 1)
	//time.Sleep(time.Duration(ctrl.delay) * time.Microsecond)
	PreciserSleep(ctrl.delay * 1000)
	for (*ctrl.ctrl).Read("ALFE_I2C_STOP") != 1 {
		//time.Sleep(time.Duration(1 * time.Microsecond))
		PreciserSleep(1000)
	}
	err := (*ctrl.ctrl).Read("ALFE_I2C_ERR")
	if err == 1 {
		log.Println("AXI-IIC Error in write sequence")
		return fmt.Errorf("AXI-IIC Error in write sequence, address=0x%x,value=0x%x", address, value)
	}
	return nil
}

func (ctrl *AXIIC) Read(address uint32) (uint32, error) {

	(*ctrl.ctrl).Write("ALFE_I2C_RW", 1)
	(*ctrl.ctrl).Write("ALFE_I2C_ADDR", uint32(address))
	(*ctrl.ctrl).Write("ALFE_I2C_START", 0)
	//time.Sleep(10 * time.Microsecond)
	PreciserSleep(10000)
	(*ctrl.ctrl).Write("ALFE_I2C_START", 1)
	//time.Sleep(time.Duration(ctrl.delay) * time.Microsecond)
	PreciserSleep(ctrl.delay * 1000)

	for (*ctrl.ctrl).Read("ALFE_I2C_STOP") != 1 {
		//time.Sleep(time.Duration(1 * time.Microsecond))
		PreciserSleep(ctrl.delay * 1000)
	}

	err := (*ctrl.ctrl).Read("ALFE_I2C_ERR")
	if err == 1 {
		log.Println("AXI-IIC Error in Read sequence")
		return 0, fmt.Errorf("AXI-IIC Error in Read sequence, address=0x%x", address)
	}
	value := (*ctrl.ctrl).Read("ALFE_I2C_READ")
	return value, nil
}

// func (a *AXIIC) Tx(addr uint16, w, r []byte) error {

// 	var err uint32

// 	//Write first
// 	for _, b := range w {

// 		a.ctrl.Write("ALFE_I2C_RW", 0)
// 		a.ctrl.Write("ALFE_I2C_ADDR", uint32(addr))
// 		a.ctrl.Write("ALFE_I2C_WRITE", uint32(b))
// 		a.ctrl.Write("ALFE_I2C_START", 0)
// 		a.ctrl.Write("ALFE_I2C_START", 1)
// 		time.Sleep(time.Millisecond)

// 		for a.ctrl.Read("ALFE_I2C_STOP") != 1 {
// 			time.Sleep(time.Duration(10 * time.Microsecond))
// 		}
// 		err = a.ctrl.Read("ALFE_I2C_ERR")
// 		if err == 1 {
// 			log.Println("AXI-IIC Write Error")
// 		}
// 	}

// 	//Read after

// 	for i := range r {
// 		a.ctrl.Write("ALFE_I2C_RW", 1)
// 		a.ctrl.Write("ALFE_I2C_ADDR", uint32(addr))
// 		a.ctrl.Write("ALFE_I2C_START", 0)
// 		a.ctrl.Write("ALFE_I2C_START", 1)
// 		err = a.ctrl.Read("ALFE_I2C_ERR")
// 		if err == 1 {
// 			log.Println("AXI-IIC Write Error")
// 		}
// 		value := a.ctrl.Read("ALFE_I2C_READ")
// 		r[i] = byte(value)
// 	}
// 	if err == 1 {
// 		return errors.New("error writing to I2C via AXI and ALFE2 custom ip")
// 	} else {
// 		return nil
// 	}
// 	return nil
// }
