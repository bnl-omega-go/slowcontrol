package SlowControl

import (
	"errors"
	"fmt"
	"log"
	"os"
)

func Exists(name string) bool {
	_, err := os.Stat(name)
	if err == nil {
		return true
	}
	if errors.Is(err, os.ErrNotExist) {
		return false
	}
	return false
}

type SysfsControllerInterface interface {
	Read(name string) int64
	Write(name string, value int64)
	ReadAll() map[string]int64
}

type SysfsDeviceController struct {
	SysfsControllerInterface
	Path      string
	Registers []string
}

func (ctrl *SysfsDeviceController) Read(name string) string {

	regname := ctrl.Path + "/" + name
	dat, err := os.ReadFile(regname)
	if err != nil {
		log.Fatal(err)
	}
	value := string(dat)
	return value
}

func (ctrl *SysfsDeviceController) Write(name string, value int64) {
	regname := ctrl.Path + "/" + name
	f, err := os.OpenFile(regname, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		log.Fatal(err)
	}
	_, err = f.Write([]byte(fmt.Sprintf("%v", value)))
	if err != nil {
		log.Fatal(err)
	}
}

func (ctrl *SysfsDeviceController) ReadAll() map[string]string {

	retval := make(map[string]string, len(ctrl.Registers))

	for _, reg := range ctrl.Registers {
		retval[reg] = ctrl.Read(reg)
		log.Printf("Register %v : %v", reg, retval[reg])
	}
	return retval
}

func NewUCD9000Controller(path string) *SysfsDeviceController {

	ctrl := SysfsDeviceController{Path: path}
	for i := 1; i <= 12; i++ {
		if Exists(ctrl.Path + "/" + fmt.Sprintf("in%v_label", i)) {
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("in%v_label", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("in%v_input", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("in%v_min", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("in%v_max", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("in%v_lcrit", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("in%v_crit", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("in%v_min_alarm", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("in%v_max_alarm", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("in%v_lcrit_alarm", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("in%v_crit_alarm", i))
		}
		if Exists(ctrl.Path + "/" + fmt.Sprintf("curr%v_label", i)) {
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("curr%v_label", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("curr%v_input", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("curr%v_max", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("curr%v_lcrit", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("curr%v_crit", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("curr%v_max_alarm", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("curr%v_crit_alarm", i))
		}
	}
	for i := 1; i <= 2; i++ {
		if Exists(ctrl.Path + "/" + fmt.Sprintf("temp%v_label", i)) {

			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("temp%v_input", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("temp%v_max", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("temp%v_crit", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("temp%v_max_alarm", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("temp%v_crit_alarm", i))
		}
	}

	for i := 1; i <= 4; i++ {
		if Exists(ctrl.Path + "/" + fmt.Sprintf("fan%v_label", i)) {
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("fan%v_input", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("fan%v_alarm", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("fan%v_fault", i))
		}
	}
	return &ctrl
}

func NewMax6699Controller(path string) *SysfsDeviceController {

	ctrl := SysfsDeviceController{Path: path}
	for i := 0; i <= 8; i++ {
		if Exists(ctrl.Path + "/" + fmt.Sprintf("temp%v_input", i)) {
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("temp%v_input", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("temp%v_max", i))
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("temp%v_max_alarm", i))
		}
		if Exists(ctrl.Path + "/" + fmt.Sprintf("temp%v_crit", i)) {
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("temp%v_crit", i))
		}
		if Exists(ctrl.Path + "/" + fmt.Sprintf("temp%v_crit_alarm", i)) {
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("temp%v_crit_alarm", i))
		}
		if Exists(ctrl.Path + "/" + fmt.Sprintf("temp%v_fault", i)) {
			ctrl.Registers = append(ctrl.Registers, fmt.Sprintf("temp%v_fault", i))
		}
	}
	return &ctrl
}

func NewINA2XXController(path string) *SysfsDeviceController {

	ctrl := SysfsDeviceController{Path: path}
	ctrl.Registers = append(ctrl.Registers, "in0_input")
	ctrl.Registers = append(ctrl.Registers, "in1_input")
	ctrl.Registers = append(ctrl.Registers, "curr1_input")
	ctrl.Registers = append(ctrl.Registers, "power1_input")
	ctrl.Registers = append(ctrl.Registers, "shunt_resistor")
	if Exists(ctrl.Path + "/" + "update_interval") {
		ctrl.Registers = append(ctrl.Registers, "update_interval")
	}
	return &ctrl
}
