//go:build linux || darwin
// +build linux darwin

package SlowControl

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"math"
)

type SamplingSpeed uint

const (
	T50us SamplingSpeed = iota
	T84us
	T150us
	T280us
	T540us
	T1052us
	T2074us
	T4120us
)

type SampleNumber uint

const (
	S1 SampleNumber = iota
	S4
	S16
	S64
	S128
	S256
	S512
	S1024
)

func FromTwoComplement(value int, length int) int {
	mask := uint(Thermometer(length))
	sign := (uint(value) & mask) >> (length - 1)
	//log.Printf("sign %v, value %b ", sign, value)
	value_inv := uint(value)

	if sign == 1 {
		value_inv = (^(uint(value)) & mask) + 1
	}

	//log.Printf(" ^value %b ", value_inv)

	if sign == 1 {
		return -1 * int(value_inv)
	} else {
		return int(value_inv)
	}
}

func ToTwoComplement(value int, length int) int {

	mask := Thermometer(length)
	if value >= 0 {
		return int(value & mask)
	} else {
		value_inv := (^(uint(-1 * value)) & uint(mask)) + 1
		return int(value_inv)
	}
}

func Thermometer(value int) int {
	result := 0
	for i := 0; i < value; i++ {
		result += (1 << i)
	}
	return (result)
}

type INA228Controller struct {
	i2cctrl                         *I2CController
	verbose                         bool
	Shunt, Max_Current, Current_lsb float64
}

func NewINA228Controller(dryrun, verbose bool) INA228Controller {
	i2cctrl := I2CController{}
	if !dryrun {
		i2cctrl.Init()
	}
	ctrl := INA228Controller{i2cctrl: &i2cctrl, verbose: verbose}
	return ctrl
}

func (ctrl *INA228Controller) AddDevice(name, bus string, addr uint16) {
	ctrl.i2cctrl.AddDevice(name, addr, bus)
}

func (ctrl *INA228Controller) Configure(name string, shunt, maxcurrent float64) {

	ctrl.Current_lsb = (maxcurrent) / 524288.
	shunt_cal := shunt * 13107.2e6 * ctrl.Current_lsb
	shunt_int := int(shunt_cal)
	if ctrl.verbose {
		log.Printf("SHUNT_CAL = %v", shunt_int)
	}
	alert := 0xFB68
	//flags := 0b001000000000001
	ctrl.i2cctrl.Write(name, []byte{0x2, byte((shunt_int >> 8) & 0x3F), byte(shunt_int & 0xFF)})
	ctrl.i2cctrl.Write(name, []byte{0x1, byte((alert >> 8) & 0xFF), byte(alert & 0xFF)})
	//ctrl.i2cctrl.Write(name, []byte{0xB, byte((flags >> 8) & 0xFF), byte(flags & 0xFF)})

}

func (ctrl *INA228Controller) Reset(name string) {
	err := ctrl.i2cctrl.Write(name, []byte{0x0, 0x80, 0x00})
	if err != nil {
		log.Fatal(err)
	}
	err = ctrl.i2cctrl.Write(name, []byte{0x0, 0x00, 0x00})
	if err != nil {
		log.Fatal(err)
	}
}

func (ctrl *INA228Controller) SetAlertPolarity(name string, polarity bool) {
	var err error
	if polarity {
		flags := 0b001000000000001
		err = ctrl.i2cctrl.Write(name, []byte{0xB, byte((flags >> 8) & 0xFF), byte(flags & 0xFF)})
	} else {
		flags := 0b000000000000001
		err = ctrl.i2cctrl.Write(name, []byte{0xB, byte((flags >> 8) & 0xFF), byte(flags & 0xFF)})
	}
	if err != nil {
		log.Fatal(err)
	}
}

func (ctrl *INA228Controller) ConfigureADC(name string, sampling_speed SamplingSpeed, samplenumber SampleNumber) {
	reg_value := uint(samplenumber&0b111) + uint((sampling_speed&0b111)<<3) + uint((sampling_speed&0b111)<<6) + uint((sampling_speed&0b111)<<9)

	err := ctrl.i2cctrl.Write(name, []byte{0x1, byte((reg_value >> 8) & 0xFF), byte(reg_value & 0xFF)})

	if err != nil {
		log.Fatal(err)
	}
}
func (ctrl *INA228Controller) GetID(name string) string {
	manubytes, err := ctrl.i2cctrl.WriteRead(name, []byte{0x3E}, 2)

	devidbytes, err2 := ctrl.i2cctrl.WriteRead(name, []byte{0x3F}, 2)
	if (err != nil) || (err2 != nil) {
		log.Fatal(err, err2)
	}
	manu, _ := binary.ReadVarint(bytes.NewBuffer(manubytes))
	rev := uint(devidbytes[1] & 0xF)
	die := (uint(devidbytes[0]) << 4) + (uint(devidbytes[1]) >> 4)

	ID := fmt.Sprintf("Manufacturer ID : %v, Die ID : %v, Rev ID : %v", manu, die, rev)

	return ID
}

func (ctrl *INA228Controller) GetPower(name string) float64 {
	data, err := ctrl.i2cctrl.WriteRead(name, []byte{0x8}, 3)
	if err != nil {
		log.Fatal(err)
	}
	power_int := int((uint(data[2]))+(uint(data[1])<<8)+(uint(data[0])<<16)) & 0xFFFFFF
	power := 3.2 * ctrl.Current_lsb * float64(power_int)
	return power
}

func (ctrl *INA228Controller) GetVoltage(name string) float64 {
	data, err := ctrl.i2cctrl.WriteRead(name, []byte{0x5}, 3)
	if err != nil {
		log.Fatal(err)
	}
	voltage_int := (int((uint(data[2]))+(uint(data[1])<<8)+(uint(data[0])<<16)) & 0xFFFFFF) >> 4
	voltage := 196.1325e-6 * float64(FromTwoComplement(voltage_int, 20))
	return voltage
}

func (ctrl *INA228Controller) GetShuntVoltage(name string) float64 {
	data, err := ctrl.i2cctrl.WriteRead(name, []byte{0x4}, 3)
	if err != nil {
		log.Fatal(err)
	}
	voltage_int := (int((uint(data[2]))+(uint(data[1])<<8)+(uint(data[0])<<16)) & 0xFFFFFF) >> 4
	voltage := 312.25e-9 * float64(FromTwoComplement(voltage_int, 20))
	return voltage
}

func (ctrl *INA228Controller) GetCurrent(name string) float64 {
	data, err := ctrl.i2cctrl.WriteRead(name, []byte{0x7}, 3)
	if err != nil {
		log.Fatal(err)
	}
	if ctrl.verbose {
		log.Printf("current data  = %x", data)
	}
	current_int := (int((uint(data[2]))+(uint(data[1])<<8)+(uint(data[0])<<16)) & 0xFFFFFF) >> 4
	current := ctrl.Current_lsb * float64(FromTwoComplement(current_int, 20))

	return current
}

func (ctrl *INA228Controller) GetTemperature(name string) float64 {
	data, err := ctrl.i2cctrl.WriteRead(name, []byte{0x6}, 2)
	if err != nil {
		log.Fatal(err)
	}
	if ctrl.verbose {
		log.Printf("temp data : %x", data)
	}
	temp_int := (int((uint(data[1]))+(uint(data[0])<<8)) & 0xFFFF)
	temp := 7.8125e-3 * float64(FromTwoComplement(temp_int, 16))
	return temp
}

func (ctrl *INA228Controller) SetBusOverVoltage(name string, value float64) {
	reg_value := int(math.Ceil(value / 3.125e-3))
	if ctrl.verbose {
		log.Printf("Setting Bus Over Voltage to %v, (%v V)", reg_value, value)
	}
	ctrl.i2cctrl.Write(name, []byte{0xE, byte((reg_value >> 8) & 0x3F), byte(reg_value & 0xFF)})
}

func (ctrl *INA228Controller) SetBusUnderVoltage(name string, value float64) {
	reg_value := int(math.Ceil(value / 3.125e-3))
	if ctrl.verbose {
		log.Printf("Setting Bus Under Voltage to %v (%v V)", reg_value, value)
	}
	ctrl.i2cctrl.Write(name, []byte{0xF, byte((reg_value >> 8) & 0x3F), byte(reg_value & 0xFF)})
}

func (ctrl *INA228Controller) SetShuntOverVoltage(name string, value float64) {
	//log.Println(value/5e-6, math.Ceil(value/5e-6), ToTwoComplement(int(math.Ceil(value/5e-6)), 16))
	reg_value := ToTwoComplement(int(math.Ceil(value/5e-6)), 16)
	if ctrl.verbose {
		log.Printf("Setting Shunt Over Voltage to %v (%v V)", reg_value, value)
	}
	ctrl.i2cctrl.Write(name, []byte{0xC, byte((reg_value >> 8) & 0xFF), byte(reg_value & 0xFF)})

}

func (ctrl *INA228Controller) SetShuntUnderVoltage(name string, value float64) {
	reg_value := ToTwoComplement(int(math.Ceil(value/5e-6)), 16)
	if ctrl.verbose {
		log.Printf("Setting Shunt Under Voltage to %v (%v V)", reg_value, value)
	}
	ctrl.i2cctrl.Write(name, []byte{0xD, byte((reg_value >> 8) & 0xFF), byte(reg_value & 0xFF)})
}

func (ctrl *INA228Controller) SetTemperatureLimit(name string, value float64) {
	reg_value := ToTwoComplement(int(math.Ceil(value/7.8125e-3)), 16)
	if ctrl.verbose {
		log.Printf("Setting Temperature limit to %v, (%v °C)", reg_value, value)
	}
	ctrl.i2cctrl.Write(name, []byte{0x10, byte((reg_value >> 8) & 0xFF), byte(reg_value & 0xFF)})
}

func (ctrl *INA228Controller) SetPowerLimit(name string, value float64) {
	reg_value := int(math.Ceil(value / (256 * 3.2 * ctrl.Current_lsb)))
	if ctrl.verbose {
		log.Printf("Setting Power limit to %v (%v W)", reg_value, value)
	}
	ctrl.i2cctrl.Write(name, []byte{0x11, byte((reg_value >> 8) & 0xFF), byte(reg_value & 0xFF)})
}
func (ctrl *INA228Controller) ReadbackLimits(name string) {
	datasol, _ := ctrl.i2cctrl.WriteRead(name, []byte{0xC}, 2)
	datasul, _ := ctrl.i2cctrl.WriteRead(name, []byte{0xD}, 2)
	databol, _ := ctrl.i2cctrl.WriteRead(name, []byte{0xE}, 2)
	databul, _ := ctrl.i2cctrl.WriteRead(name, []byte{0xF}, 2)
	datatm, _ := ctrl.i2cctrl.WriteRead(name, []byte{0x10}, 2)
	datapm, _ := ctrl.i2cctrl.WriteRead(name, []byte{0x11}, 2)

	sol := float64(FromTwoComplement((int(datasol[0])<<8)+int(datasol[1]), 16)) * 5e-6
	sul := float64(FromTwoComplement((int(datasul[0])<<8)+int(datasul[1]), 16)) * 5e-6
	bol := float64((int(databol[0])<<8)+int(databol[1])) * 3.125e-3
	bul := float64((int(databul[0])<<8)+int(databul[1])) * 3.125e-3
	tm := float64(FromTwoComplement((int(datatm[0])<<8)+int(datatm[1]), 16)) * 7.8125e-3
	pm := float64((int(datapm[0])<<8)+int(datapm[1])) * 256 * 3.2 * ctrl.Current_lsb

	log.Printf("Limits readback : Shunt UV|OV = %v | %v V, Bus UV|OV = %v | %v V, T Max = %v °C, Power Max = %v W", sul, sol, bul, bol, tm, pm)
}

func (ctrl *INA228Controller) GetAlerts(name string) {
	data, err := ctrl.i2cctrl.WriteRead(name, []byte{0xb}, 2)
	if err != nil {
		log.Println(err)
	}
	alerts := (uint(data[0]) >> 8) + uint(data[1])

	if ctrl.verbose {
		log.Printf("Raw ALERT Data %x", alerts)
	}
	pol := (alerts >> 2) & 0b1
	busul := (alerts >> 3) & 0b1
	busol := (alerts >> 4) & 0b1
	shntul := (alerts >> 5) & 0b1
	shntol := (alerts >> 6) & 0b1
	tmpol := (alerts >> 7) & 0b1

	log.Printf("Alert status : TempOL : %v, Bus UL : %v OL: %v, Shunt UL : %v OL: %v, Power OL : %v ", tmpol, busul, busol, shntul, shntol, pol)

}
