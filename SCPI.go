package SlowControl

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"
	"time"
)

type Instrument struct {
	Name       string
	Ip_address string
	conn       *net.TCPConn
	addr       *net.TCPAddr
	wrbufio    *bufio.Writer
}

type SCPIController struct {
	Instruments map[string]*Instrument
}

func (scpi *SCPIController) Init() (err error) {
	scpi.Instruments = make(map[string]*Instrument)
	return
}

func (scpi *SCPIController) ConnectInstrument(name, ip_adress string, port int) (err error) {

	instr := Instrument{Name: name, Ip_address: fmt.Sprintf("%v:%v", ip_adress, port)}
	instr.addr, err = net.ResolveTCPAddr("tcp", instr.Ip_address)
	if err != nil {
		return
	}

	instr.conn, err = net.DialTCP("tcp", nil, instr.addr)

	instr.wrbufio = bufio.NewWriter(instr.conn)
	scpi.Instruments[name] = &instr

	return
}

func (scpi *SCPIController) Write(name, command string) (err error) {
	cmd := fmt.Sprintf("%s\n", command)
	_, err = scpi.Instruments[name].wrbufio.Write([]byte(cmd))
	if err != nil {
		log.Println(err)
	}
	//_, err = scpi.Instruments[name].conn.Write([]byte(fmt.Sprintf("%s\n", command)))
	err = scpi.Instruments[name].wrbufio.Flush()
	if err != nil {
		log.Println(err)
	}
	return
}

func (scpi *SCPIController) Read(name string, length int) (answer string, err error) {
	//buf := make([]byte, 512)
	reader := bufio.NewReader(scpi.Instruments[name].conn)
	scpi.Instruments[name].conn.SetReadDeadline(time.Now().Add(3 * time.Second))
	buf, err := reader.ReadBytes(0xA)

	if err != nil {
		log.Println(err)
	}
	answer = strings.TrimRight(string(buf), "\x00")
	answer = strings.TrimRight(answer, "\n")

	return
}

func (scpi *SCPIController) Query(name, command string, length int) (answer string, err error) {
	//log.Printf("Write %v", command)
	_, err = scpi.Instruments[name].conn.Write([]byte(command + "\n"))
	if err != nil {
		return
	}
	//log.Println("read")
	answer, err = scpi.Read(name, length)
	//log.Println("done", err)
	if err != nil {
		return
	}
	return
}

func (scpi *SCPIController) Disconnect(name string) {
	scpi.Instruments[name].conn.Close()
}
