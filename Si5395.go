//go:build linux || darwin
// +build linux darwin

package SlowControl

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

func parsehex(val string) (uint16, error) {
	var retvalint uint64
	var err error
	if strings.Contains(val, "0x") || strings.Contains(val, "0X") {
		nval := strings.Replace(val, "0x", "", -1)
		nval = strings.Replace(nval, "0X", "", -1)
		retvalint, err = strconv.ParseUint(nval, 16, 16)
		return uint16(retvalint), err
	} else {
		retvalint, err = strconv.ParseUint(val, 10, 16)
		return uint16(retvalint), err
	}
}

type Si5345Controller struct {
	i2cctrl    *I2CController
	ctrl, freq map[string][]byte
	page       byte
	verbose    bool
}

func NewSi5345Controller(addr uint16, bus string, dryrun, verbose bool) Si5345Controller {
	i2cctrl := I2CController{}
	if !dryrun {
		i2cctrl.Init()
		i2cctrl.AddDevice("si5345", addr, bus)
	}
	ctrl := Si5345Controller{i2cctrl: &i2cctrl, verbose: verbose}
	return ctrl
}

func (ctrl *Si5345Controller) BlockWrite(registers []byte, verbose bool, delay int, dryrun bool) {
	for i := 0; i < len(registers); i += 3 {
		cur_page, addr, reg := registers[i], registers[i+1], registers[i+2]

		if cur_page != ctrl.page {
			ctrl.page = cur_page
			data := []byte{0x01, ctrl.page}
			if verbose {
				log.Printf("Changing page to %x\n", ctrl.page)
			}
			if !dryrun {
				err := ctrl.i2cctrl.Write("si5345", data)
				time.Sleep(time.Duration(delay) * time.Millisecond)
				if err != nil {
					log.Fatal(err)
				}
			}
		}

		//Actual register value
		data := []byte{addr, reg}
		if verbose {
			log.Printf("Writing %x to page %v, register %x \n", reg, ctrl.page, addr)
		}
		if !dryrun {
			err := ctrl.i2cctrl.Write("si5345", data)
			time.Sleep(time.Duration(delay) * time.Millisecond)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}

func (ctrl *Si5345Controller) ReadSi5345Registers(filename string) (ctrl_registers, freq_registers map[string][]byte) {
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}

	// remember to close the file at the end of the program
	defer f.Close()

	//log.Printf("Configuring Si5345 on bus %v with Address 0x%x \n", ctrl.i2cctrl.Devices["si5345"].Bus, ctrl.i2cctrl.Devices["si5345"].Addr)

	// read csv values using csv.Reader
	csvReader := csv.NewReader(f)
	csvReader.Comment = '#'
	data, err := csvReader.ReadAll()

	if data[0][0] == "Address" {
		data = data[1:]
	}

	if err != nil {
		log.Fatal(err)
	}

	ctrl_registers = make(map[string][]byte, 1)
	ctrl_registers["preamble"] = make([]byte, 3*3)
	for n, line := range data[:3] {
		addr, err := parsehex(line[0])
		val, err2 := parsehex(line[1])
		if err != nil || err2 != nil {
			log.Fatal(err, err2)
		}
		ctrl_registers["preamble"][3*n] = byte((addr >> 8) & 0xFF)
		ctrl_registers["preamble"][3*n+1] = byte((addr) & 0xFF)
		ctrl_registers["preamble"][3*n+2] = byte(val)
	}

	ctrl_registers["postamble"] = make([]byte, 5*3)
	for n, line := range data[len(data)-5:] {
		addr, err := parsehex(line[0])
		val, err2 := parsehex(line[1])
		if err != nil || err2 != nil {
			log.Fatal(err, err2)
		}
		ctrl_registers["postamble"][3*n] = byte((addr >> 8) & 0xFF)
		ctrl_registers["postamble"][3*n+1] = byte((addr) & 0xFF)
		ctrl_registers["postamble"][3*n+2] = byte(val)
	}

	freq_registers = make(map[string][]byte, 1)
	freq_registers["default"] = make([]byte, len(data[3:len(data)-5])*3)

	for n, line := range data[3 : len(data)-5] {
		addr, err := parsehex(line[0])
		val, err2 := parsehex(line[1])
		if err != nil || err2 != nil {
			log.Fatal(err, err2)
		}
		freq_registers["default"][3*n] = byte((addr >> 8) & 0xFF)
		freq_registers["default"][3*n+1] = byte((addr) & 0xFF)
		freq_registers["default"][3*n+2] = byte(val)
	}
	return
}

func (ctrl *Si5345Controller) ConfigureClock(filename string, delay int, dryrun bool) {

	//ctrl.ctrl, ctrl.freq = GetSi5345Configs()
	ctrl.ctrl, ctrl.freq = ctrl.ReadSi5345Registers(filename)
	ctrl.page = byte(0)

	ctrl.BlockWrite(ctrl.ctrl["preamble"], ctrl.verbose, delay, dryrun)
	//Preample done , wait 625ms
	if ctrl.verbose {
		log.Println("pausing after preamble")
	}
	time.Sleep(625 * time.Millisecond)

	ctrl.BlockWrite(ctrl.freq["default"], ctrl.verbose, delay, dryrun)
	//Frequency registers set
	if ctrl.verbose {
		log.Println("pausing after frequency set")
	}
	time.Sleep(10 * time.Millisecond)

	ctrl.BlockWrite(ctrl.ctrl["postamble"], ctrl.verbose, delay, dryrun)
	if ctrl.verbose {
		log.Println("post amble completed")
	}

	log.Println("Done!")

}
