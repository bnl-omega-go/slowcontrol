//go:build linux || darwin
// +build linux darwin

package SlowControl

import (
	"encoding/csv"
	"log"
	"os"

	"periph.io/x/conn/v3/driver/driverreg"
	"periph.io/x/conn/v3/i2c"
	"periph.io/x/conn/v3/i2c/i2creg"
	"periph.io/x/conn/v3/physic"
	"periph.io/x/host/v3"
)

type i2cer interface {
	Init()
	AddDevice(name string, address uint32, bus string)
	ListDevices()
	Write(name string, register uint8, data []byte) error
	Read(name string, register uint8, length int) ([]byte, error)
	WriteRead(name string, register uint8, data []byte, length int) ([]byte, error)
	Reset()
	SetClockDivider(div uint32)
}

type I2CController struct {
	i2cer
	Devices map[string]*i2c.Dev
	state   *driverreg.State
	Bus     i2c.Bus
	base    int64
}

func (i *I2CController) Init() {
	var err error
	i.Devices = make(map[string]*i2c.Dev)
	(*i).state, err = host.Init()
	if err != nil {
		log.Fatalf("failed to initialize periph: %v", err)
	}
}

func (i *I2CController) AddDevice(name string, address uint16, bus string) {

	b, err := i2creg.Open(bus)
	b.SetSpeed(400 * physic.KiloHertz)
	if err != nil {
		log.Fatal(err)
	}
	i.Devices[name] = &i2c.Dev{Bus: b, Addr: address}

}

func (i *I2CController) GetDevices() []string {
	devlist := make([]string, len(i.Devices))
	it := 0
	for key := range i.Devices {
		devlist[it] = key
		it++
	}
	return devlist
}

func (i *I2CController) ListDevices() {
	for key, dev := range i.Devices {
		log.Printf("Device %v with address %v on bus %v", key, dev.Addr, dev.Bus)
	}
}

func (i *I2CController) Write(name string, data []byte) error {

	return i.Devices[name].Bus.Tx(i.Devices[name].Addr, data, nil)
}

func (i *I2CController) Read(name string, length int) (data []byte, err error) {

	data = make([]byte, length)
	err = i.Devices[name].Bus.Tx(i.Devices[name].Addr, nil, data)
	return
}

func (i *I2CController) WriteRead(name string, data []byte, length int) (rdata []byte, err error) {

	rdata = make([]byte, length)
	err = i.Devices[name].Bus.Tx(i.Devices[name].Addr, data, rdata)
	return
}

func (ctrl *I2CController) WritelpGBTConfiguration(filename string, verbose, dryrun bool) {

	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	// remember to close the file at the end of the program
	defer f.Close()

	// read csv values using csv.Reader
	csvReader := csv.NewReader(f)
	csvReader.Comment = '#'
	csvReader.Comma = ' '
	data, _ := csvReader.ReadAll()

	for _, reg := range data {
		address, _ := parsehex(reg[0])
		value, _ := parsehex(reg[1])

		if verbose {
			log.Printf("Writing register %x = %x", address, value)
		}

		reg_add_l := address & 0xFF
		reg_add_h := (address >> 8) & 0xFF
		payload := []byte{byte(reg_add_l), byte(reg_add_h), byte(value)}

		if !dryrun {
			err = ctrl.Write("lpGBT", payload)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}
