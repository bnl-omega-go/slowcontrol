# Slow Control Library 

The slow control library is a collection of useful interface for developping with linux system and FPGA. 


## I2C Controller 

The I2C Controller is a convenient structure to control devices accesible via I2C through the linux kernel drivers. 

The devices connected to the I2C buses can be made know to the controller by adding them to the know device list, and can then be interfaced by name : 

```go
package main 

import(
    	SlowControl "gitlab.cern.ch/bnl-omega-go/slowcontrol"
        "log"
)


func main(){

//Initialize the controller
ctrl := SlowControl.I2CController{}
ctrl.Init()

//Add your device to the known list 
mydevaddr:=uint16(0xA)
ctrl.AddDevice("myDevice", mydevaddr, "/dev/i2c-1")}

//Write 2 bytes to device
ctrl.Write("myDevice",[]byte{0xBE,0XEF})

//Write 2 bytes, readback 3 bytes after, readback is a byte list
readback : ctrl.WriteRead("myDevice",[]byte{0xBE,0xEF},3)

log.Printf("read bytes : %v",readback)
}

```


### Si5345 Controller 

The Si5345 Controller is a higher level I2C device Controller that implements the function to read configuration files and program a Si5345 device connected to the bus : 

```go
package main

import (
	"log"

	SlowControl "gitlab.cern.ch/bnl-omega-go/slowcontrol"
)

func main() {

    devaddr := uint16(0xA)
    i2cbus := "/dev/i2c-2"
    dryrun:=false
    verbose:=true
    delay:=1
    conffile:="si5345.csv"
	
    ctrl := SlowControl.NewSi5345Controller(devaddr, i2cbus, dryrun, verbose)
	ctrl.ConfigureClock(conffile, delay, dryrun)

```

See this [application](https://gitlab.cern.ch/bnl-omega-go/si5345tool) for more details. 

### INA228 Controller

The INA228 Controller is a higher level I2C device Controller that implements the function to read and configure the INA228 devices connected to the bus. See this [application](https://gitlab.cern.ch/bnl-omega-go/ina228tool) for example of usage.




## AXI-I2C Controller 

The AXI-I2C Controller is a special flavor of I2C driver for the BNL custom I2C FW IP for ALFE2 characterization. 

the AXIIIC Controller implements the I2C interface with its basic functions : 

```go 
package main

import (
	"log"

	SlowControl "gitlab.cern.ch/bnl-omega-go/slowcontrol"
)

func main() {

        base := 0x00A0020000 //Memory base for IP
        delay := 1 // delay in waiting for answer

        axiictrl := SlowControl.NewAXIIC(base, delay)

        //Set clock divider to 250kHz
        axiictrl.SetClockDivider(0b111)
        
        //Write 0xAB to bus device address 0xA
        axiictrl.Write(0xA,0xAB)

        //Read Device 0xA 
        value := axiictrl.Read(0xA)
}

```


### ALFE2 Controller 

The ALFE2 Controller is higher level device driver based on the AXI-I2C driver. It is used to configure the ALFE2 ASIC connected to the FW IP. 

To see a full example of its usage, see this [application](https://gitlab.cern.ch/BNL-ATLAS/larphase2/fetb2/gofetb2daq/-/blob/master/Exec/FETB2ALFE2Tool.go).



# SCPI Controller

The SCPI controller is an interface to SCPI compliant instruments via TCP/IP connection. It alllow to send and receive messages from the SCPI device : 


```go 
package main

import (
	"log"

	SlowControl "gitlab.cern.ch/bnl-omega-go/slowcontrol"
)

func main() {

   ctrl:= SlowControl.SCPIController{}

   err:= ctrl.Init()

   if err!=nil{
        log.Println(err)
   }

   // Connect device, give a name and provide ip and port 
   err = ctrl.ConnectInstrument("myDevice","123.123.123.123",8080)
   if err!=nil{
        log.Println(err)
   }

   //Write reset command to device
   err = ctrl.Write("myDevice","RST*")
   if err!=nil{
        log.Println(err)
   }
    //A query is command sent with expected answer
   readback, err2 := ctrl.Query("myDevice","IDN?")
      if err2!=nil{
        log.Println(err2)
   }
}

```


## E36300 Controller

The E36300 Control is a higher level device driver for the Keysight E36300 familly of poeer supplies. It provides the basic functionalities to configure and read the data from the supplies. It make use of the SCPI driver. 

```go 
package main

import (
	"log"

	SlowControl "gitlab.cern.ch/bnl-omega-go/slowcontrol"
)

func main() {

    supplies := SlowControl.PowerSupplies{}
    
    //You must read a YAML file describing the setup before initialization 
    supplies.ReadYAML("supplies.yaml")
    supplies.Init()

    //measure all specified channel in YAML (V+I)
    measure := supplies.Measure()

    log.Println("Measurements : ",measure)
   
}


```

The Controller is configured via a YAML file that described the connect power supplies. See example below : 

```yaml
supplies:
    ALFE2_1:
        channels:
            3V_SH:
                voltage: 3
                currentlimit: 0.1
                name: 3V_SH
                id: 3
            3V_VDA:
                voltage: 3
                currentlimit: 0.1
                name: 3V_VDA
                id: 2
            3V_VDD:
                voltage: 3
                currentlimit: 0.2
                name: 3V_VDD
                id: 1
        ip: 192.168.1.202
        port: 5025
    ALFE2_2:
        channels:
            3V_VDA2:
                voltage: 3
                currentlimit: 0.32
                name: 3V_VDA2
                id: 2
            3V_VDD2:
                voltage: 3
                currentlimit: 0.15
                name: 3V_VDD2
                id: 1
            4V:
                voltage: 4
                currentlimit: 0.25
                name: 4V
                id: 3
        ip: 192.168.1.203
        port: 5025
    Pulser:
        channels:
            +6V:
                voltage: 6
                currentlimit: 0.4
                name: +6V
                id: 1
            +11V:
                voltage: 11
                currentlimit: 0.05
                name: +11V
                id: 3
            -6V:
                voltage: 6
                currentlimit: 0.2
                name: -6V
                id: 2
        ip: 192.168.1.201
        port: 5025


```





# Preciser Sleep function 

The Precise Sleep function is implemented ty bypass the limited granularity of the go timer in arm64 linux architecture. It should be used instead of go ```time.Sleep()``` function. You can expect precision of the order of 1 µs. 

```go 
package main

import (
	"log"

	SlowControl "gitlab.cern.ch/bnl-omega-go/slowcontrol"
)

func main() {

        //Sleep 1000 nanosecond 
        SlowControl.PreciserSleep(1000)
}

```