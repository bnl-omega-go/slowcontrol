//go:build linux || darwin
// +build linux darwin

package SlowControl

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"

	yaml "gopkg.in/yaml.v3"
)

type Measurement struct {
	Voltage, Current float64
}

type E36300_Channel struct {
	Voltage      float64
	CurrentLimit float64
	Name         string
	ID           int
}

type E36300 struct {
	Channels map[string]E36300_Channel
	IP       string
	Port     int
}

type PowerSupplies struct {
	Supplies map[string]E36300
	scpi     SCPIController
}

func (scan *PowerSupplies) ReadYAML(filename string) {

	yfile, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	scany := PowerSupplies{}
	err2 := yaml.Unmarshal(yfile, &scan)

	if err2 != nil {
		log.Fatal(err2)
	}
	scan = &scany
}

func (scan *PowerSupplies) WriteYAML(filename string) {
	data, err := yaml.Marshal(scan)
	if err != nil {
		log.Fatal(err)
	}
	err2 := ioutil.WriteFile(filename, data, 0666)
	if err2 != nil {
		log.Fatal(err2)
	}
}

func (supplies *PowerSupplies) Init() error {

	supplies.scpi.Init()

	for name, inst := range supplies.Supplies {
		err := supplies.scpi.ConnectInstrument(name, inst.IP, inst.Port)
		err1 := supplies.scpi.Write(name, "CLS*")

		if err != nil {
			return err
		}
		if err1 != nil {
			return err1
		}
	}

	return nil
}

func (supplies *PowerSupplies) SetupChannels() error {
	for name, inst := range supplies.Supplies {
		for _, ch := range inst.Channels {
			cmd := fmt.Sprintf("VOL %v,(@%v)", ch.Voltage, ch)
			err1 := supplies.scpi.Write(name, cmd)
			cmd = fmt.Sprintf("APPLY CH%v,%v,%v", ch.ID, ch.Voltage, ch.CurrentLimit)
			err2 := supplies.scpi.Write(name, cmd)
			if err1 != nil {
				return err1
			}
			if err2 != nil {
				return err2
			}
		}
	}
	return nil
}

func (supplies *PowerSupplies) SwitchAllOn() error {

	for name, inst := range supplies.Supplies {
		for _, ch := range inst.Channels {
			cmd := fmt.Sprintf("OUTP ON,(@%v)", ch.ID)
			err := supplies.scpi.Write(name, cmd)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (supplies *PowerSupplies) SwitchAllOff() error {

	for name, inst := range supplies.Supplies {
		for _, ch := range inst.Channels {
			cmd := fmt.Sprintf("OUTP ON,(@%v)", ch.ID)
			err := supplies.scpi.Write(name, cmd)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (supplies *PowerSupplies) Measure() (map[string]Measurement, error) {

	result := make(map[string]Measurement)

	for name, inst := range supplies.Supplies {
		for chname, ch := range inst.Channels {
			cmd := fmt.Sprintf("MEAS:CURR? (@%v)", ch.ID)
			resp, err := supplies.scpi.Query(name, cmd, 50)
			if err != nil {
				return nil, err
			}
			current, error := strconv.ParseFloat(resp, 64)
			if error != nil {
				log.Println(error)
			}
			cmd = fmt.Sprintf("MEAS:VOLT? (@%v)", ch.ID)
			resp, err = supplies.scpi.Query(name, cmd, 50)
			if err != nil {
				return nil, err
			}
			voltage, error2 := strconv.ParseFloat(resp, 64)
			if error2 != nil {
				log.Println(error)
			}
			result[fmt.Sprintf("%v_%v", name, chname)] = Measurement{Voltage: voltage, Current: current}

		}
	}
	return result, nil
}
