//go:build linux || darwin
// +build linux darwin

package SlowControl

import (
	"fmt"
	"log"

	"periph.io/x/conn/v3/driver/driverreg"
	"periph.io/x/conn/v3/physic"
	"periph.io/x/conn/v3/spi"
	"periph.io/x/conn/v3/spi/spireg"
	host "periph.io/x/host/v3"
)

type spier interface {
	Init()
	AddDevice(name string, bits int)
	ListDevices()
	//	Write(name string, data []byte) error
	//	Read(name string, length int) ([]byte, error)
	Transfer(name string, data []byte, length int) ([]byte, error)
}

type SPIController struct {
	spier
	Devices map[string]*spi.Conn
	state   *driverreg.State
	pc      spi.PortCloser
}

func (i *SPIController) Init() {
	var err error
	i.Devices = make(map[string]*spi.Conn)
	(*i).state, err = host.Init()

	if err != nil {
		log.Fatalf("failed to initialize periph: %v", err)
	}
}

func (i *SPIController) AddDevice(name string, bus int, mode int) {
	var err error
	i.pc, err = spireg.Open(fmt.Sprintf("/dev/spidev%v.0", bus))
	if err != nil {
		log.Fatal(err)
	}
	//defer i.pc.Close()

	d, err2 := i.pc.Connect(physic.MegaHertz, spi.Mode0, 8)
	d.Tx([]byte{0, 0, 0}, []byte{0, 0, 0})
	i.Devices[name] = &d
	if err2 != nil {
		log.Fatal(err2)
	}
}

func (i *SPIController) Transfer(name string, data []byte) ([]byte, error) {

	readback := []byte{0, 0, 0}
	err := (*i.Devices[name]).Tx(data, readback)
	return readback, err

}

func (i *SPIController) ListDevices() {
	for key, dev := range i.Devices {
		log.Printf("Device %v  on bus %v", key, dev)
	}
}
