package SlowControl

import (
	"fmt"
	"io/ioutil"
	"log"

	yaml "gopkg.in/yaml.v3"
)

type ADCRegisterPage struct {
	SPIRegisters map[uint8][]ADCRegister
}

type ADCRegisterMap struct {
	MasterPage         ADCRegisterPage
	ADCPage            ADCRegisterPage
	DigitalChannelPage ADCRegisterPage
	CH8DigitalPage     ADCRegisterPage
	MiscPage           ADCRegisterPage
}

func (adcregmap *ADCRegisterMap) Init() {
	adcregmap.GenerateMasterPage()
	adcregmap.GenerateADCRegPage()
	adcregmap.GenerateDigitalChannelPage()
	adcregmap.Generate8CHDigitalPage()
	adcregmap.GenerateMiscPage()
}

func (adcregmap *ADCRegisterMap) GenerateMasterPage() {
	adcregmap.MasterPage = ADCRegisterPage{SPIRegisters: map[uint8][]ADCRegister{
		0x0: {
			{Name: "SOFTWARE_RESET", length: 1, shift: 0, defaultvalue: 0, read: true, write: true},
			{Name: "READ_REG_EN", length: 1, shift: 1, defaultvalue: 0, read: true, write: true},
		},
		0x1:  {{Name: "GLOBAL_PDN", length: 1, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x10: {{Name: "3WIRE_SPI_MODE", length: 1, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x11: {
			{Name: "PAGE_SEL_ADC1", length: 1, shift: 0, defaultvalue: 0, read: true, write: false},
			{Name: "PAGE_SEL_ADC2", length: 1, shift: 1, defaultvalue: 0, read: true, write: false},
			{Name: "PAGE_SEL_ADC3", length: 1, shift: 2, defaultvalue: 0, read: true, write: false},
			{Name: "PAGE_SEL_ADC4", length: 1, shift: 3, defaultvalue: 0, read: true, write: false},
			{Name: "PAGE_SEL_ADC5", length: 1, shift: 4, defaultvalue: 0, read: true, write: false},
			{Name: "PAGE_SEL_ADC6", length: 1, shift: 5, defaultvalue: 0, read: true, write: false},
			{Name: "PAGE_SEL_ADC7", length: 1, shift: 6, defaultvalue: 0, read: true, write: false},
			{Name: "PAGE_SEL_ADC8", length: 1, shift: 7, defaultvalue: 0, read: true, write: false},
		},
		0x12: {
			{Name: "PAGE_SEL_8CH_DIG", length: 1, shift: 0, defaultvalue: 0, read: true, write: true},
			{Name: "PAGE_SEL_MISC", length: 1, shift: 1, defaultvalue: 0, read: true, write: true},
		},
		0x13: {
			{Name: "PAGE_SEL_DIG1", length: 1, shift: 0, defaultvalue: 0, read: true, write: true},
			{Name: "PAGE_SEL_DIG2", length: 1, shift: 1, defaultvalue: 0, read: true, write: true},
			{Name: "PAGE_SEL_DIG3", length: 1, shift: 2, defaultvalue: 0, read: true, write: true},
			{Name: "PAGE_SEL_DIG4", length: 1, shift: 3, defaultvalue: 0, read: true, write: true},
			{Name: "PAGE_SEL_DIG5", length: 1, shift: 4, defaultvalue: 0, read: true, write: true},
			{Name: "PAGE_SEL_DIG6", length: 1, shift: 5, defaultvalue: 0, read: true, write: true},
			{Name: "PAGE_SEL_DIG7", length: 1, shift: 6, defaultvalue: 0, read: true, write: true},
			{Name: "PAGE_SEL_DIG8", length: 1, shift: 7, defaultvalue: 0, read: true, write: true},
		},
		0x1E: {{Name: "JESD_COMMON_SPI_MODE", length: 1, shift: 1, defaultvalue: 0, read: true, write: true}},
	}}
}

func (adcregmap *ADCRegisterMap) GenerateADCRegPage() {
	adcregmap.ADCPage = ADCRegisterPage{SPIRegisters: map[uint8][]ADCRegister{
		0x20: {{Name: "PDN_ADC", length: 8, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x25: {{Name: "INIT_BIT", length: 1, shift: 1, defaultvalue: 0, read: true, write: true}},
	}}
}

func (adcregmap *ADCRegisterMap) GenerateDigitalChannelPage() {
	adcregmap.DigitalChannelPage = ADCRegisterPage{SPIRegisters: map[uint8][]ADCRegister{
		0x20: {
			{Name: "DIG_CH_EN", length: 1, shift: 0, defaultvalue: 0, read: true, write: true},
			{Name: "DIF_OFFSET_EN", length: 1, shift: 1, defaultvalue: 0, read: true, write: true},
			{Name: "AVG_EN", length: 1, shift: 2, defaultvalue: 0, read: true, write: true},
			{Name: "DIG_HPF_EN", length: 1, shift: 3, defaultvalue: 0, read: true, write: true},
			{Name: "DIG_GAIN_EN", length: 1, shift: 4, defaultvalue: 0, read: true, write: true},
		},
		0x21: {
			{Name: "OFFSET_REMOVAL_SELF", length: 1, shift: 0, defaultvalue: 0, read: true, write: true},
			{Name: "OFFSET_REMOVAL_START_SEL", length: 1, shift: 1, defaultvalue: 0, read: true, write: true},
			{Name: "OFFSET_REMOVAL_START_MANUAL", length: 1, shift: 2, defaultvalue: 0, read: true, write: true},
			{Name: "AUTO_OFFSET_REMOVAL_ACC_CYCLES", length: 4, shift: 3, defaultvalue: 0, read: true, write: true},
			{Name: "OFFSET_CORR_DELAY_FROM_TX_TRIG", length: 8, shift: 7, defaultvalue: 0, read: true, write: true},
		},
		0x22: {{Name: "OFFSET_ADC", length: 10, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x23: {{Name: "AUTO_OFFSET_REMOVAL_VAL_RD", length: 16, shift: 0, defaultvalue: 0, read: true, write: false}},
		0x24: {
			{Name: "HPF_ROUND_EN", length: 1, shift: 0, defaultvalue: 0, read: true, write: true},
			{Name: "HPF_REG_PROG_EN", length: 1, shift: 1, defaultvalue: 0, read: true, write: true},
		},
		0x25: {{Name: "HPF_GAIN", length: 16, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x26: {{Name: "HPF_K1", length: 16, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x27: {{Name: "HPF_K2", length: 16, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x28: {{Name: "GAIN_ADC", length: 5, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x29: {{Name: "TEST_PAT_MODE", length: 4, shift: 1, defaultvalue: 0, read: true, write: true}},
		0x2A: {{Name: "CUSTOM_PATTERN1", length: 16, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x2B: {{Name: "CUSTOM_PATTERN2", length: 16, shift: 0, defaultvalue: 0, read: true, write: true}},
	}}
}

func (adcregmap *ADCRegisterMap) Generate8CHDigitalPage() {

	adcregmap.CH8DigitalPage = ADCRegisterPage{SPIRegisters: map[uint8][]ADCRegister{
		0x28: {{Name: "LOW_LATENCY_EN", length: 1, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x29: {{Name: "EN_DEMOD", length: 1, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x2B: {
			{Name: "CFG_TX_TRIG_DELAY", length: 5, shift: 0, defaultvalue: 0, read: true, write: true},
			{Name: "CFG_INPUT_DATA_DELAY", length: 5, shift: 5, defaultvalue: 0, read: true, write: true},
			{Name: "CFG_OUTOUT_DATA_DELAY", length: 5, shift: 5, defaultvalue: 0, read: true, write: true},
		},
		0x2C: {{Name: "EN_1L_MODE2", length: 1, shift: 3, defaultvalue: 0, read: true, write: true}},
		0x2F: {
			{Name: "LANE1_MUX_SEL", length: 1, shift: 0, defaultvalue: 0, read: true, write: true},
			{Name: "LANE2_MUX_SEL", length: 1, shift: 1, defaultvalue: 0, read: true, write: true},
			{Name: "LANE3_MUX_SEL", length: 1, shift: 2, defaultvalue: 0, read: true, write: true},
			{Name: "LANE4_MUX_SEL", length: 1, shift: 3, defaultvalue: 0, read: true, write: true},
			{Name: "EN_LOW_INTERFACE_RATE", length: 15, shift: 1, defaultvalue: 0, read: true, write: true},
		},
		0x31: {
			{Name: "TX_LINK_DIS", length: 1, shift: 0, defaultvalue: 0, read: true, write: true},
			{Name: "FRAME_ALIGN", length: 1, shift: 1, defaultvalue: 0, read: true, write: true},
			{Name: "LANE_ALIGN", length: 1, shift: 2, defaultvalue: 0, read: true, write: true},
			{Name: "TRANSPORT_LAYER_TEST_MODE_EN", length: 1, shift: 4, defaultvalue: 0, read: true, write: true},
			{Name: "CTRL_MODE", length: 1, shift: 6, defaultvalue: 0, read: true, write: true},
			{Name: "CTRL_K", length: 1, shift: 7, defaultvalue: 0, read: true, write: true},
			{Name: "PLL_MODE", length: 2, shift: 8, defaultvalue: 0, read: true, write: true},
			{Name: "SYNC_REG_EN", length: 1, shift: 14, defaultvalue: 0, read: true, write: true},
			{Name: "SYNC_REG", length: 1, shift: 15, defaultvalue: 0, read: true, write: true},
		},
		0x32: {
			{Name: "LMFC_MASK_RES", length: 1, shift: 3, defaultvalue: 0, read: true, write: true},
			{Name: "LINK_LAY_RPAT", length: 1, shift: 4, defaultvalue: 0, read: true, write: true},
			{Name: "LIN_LAYER_TEST_MODE", length: 3, shift: 5, defaultvalue: 0, read: true, write: true},
			{Name: "REL_ILA_SEQ", length: 2, shift: 8, defaultvalue: 0, read: true, write: true},
			{Name: "EN_1L_MODE0", length: 1, shift: 15, defaultvalue: 0, read: true, write: true},
		},
		0x33: {
			{Name: "DEVICE_ID", length: 8, shift: 0, defaultvalue: 0, read: true, write: true},
			{Name: "BANK_ID", length: 4, shift: 8, defaultvalue: 0, read: true, write: true},
			{Name: "SCRAMBLE_EN", length: 1, shift: 15, defaultvalue: 0, read: true, write: true},
		},
		0x34: {
			{Name: "FRAME_PER_MULTIFRAME(K)", length: 5, shift: 0, defaultvalue: 0, read: true, write: true},
			{Name: "JESD_VER", length: 3, shift: 8, defaultvalue: 0, read: true, write: true},
			{Name: "JESD_SUBCLASS", length: 3, shift: 11, defaultvalue: 0, read: true, write: true},
		},
		0x35: {
			{Name: "CTRL_S", length: 1, shift: 4, defaultvalue: 0, read: true, write: true},
			{Name: "CTRL_M", length: 1, shift: 6, defaultvalue: 0, read: true, write: true},
			{Name: "CTRL_L", length: 1, shift: 7, defaultvalue: 0, read: true, write: true},
			{Name: "NUMBER_OF_LANE_PER_LINK(L)", length: 5, shift: 8, defaultvalue: 0, read: true, write: true},
		},
		0x36: {{Name: "NUMBER_OF_CONVERTER_PER_DEVICE(M)", length: 8, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x37: {{Name: "NUMBER_OF_SAMPLE_PER_FRAME(S)", length: 5, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x5D: {{Name: "EN_1L_MODE", length: 1, shift: 15, defaultvalue: 0, read: true, write: true}},
		0x5F: {
			{Name: "PDN_PLL_JESD1", length: 1, shift: 3, defaultvalue: 0, read: true, write: true},
			{Name: "PDN_JESD_LANE_1_2", length: 4, shift: 4, defaultvalue: 0, read: true, write: true},
			{Name: "PDN_JESD1", length: 1, shift: 9, defaultvalue: 0, read: true, write: true},
		},
		0x60: {
			{Name: "SEL_EMP_LANE1", length: 6, shift: 1, defaultvalue: 0, read: true, write: true},
			{Name: "SEL_EMP_LANE2", length: 6, shift: 7, defaultvalue: 0, read: true, write: true},
		},
		0x65: {
			{Name: "PDN_PLL_JESD2", length: 1, shift: 3, defaultvalue: 0, read: true, write: true},
			{Name: "PDN_JESD_LANE_3_4", length: 4, shift: 4, defaultvalue: 0, read: true, write: true},
			{Name: "PDN_JESD2", length: 1, shift: 9, defaultvalue: 0, read: true, write: true},
		},
		0x66: {
			{Name: "SEL_EMP_LANE3", length: 6, shift: 1, defaultvalue: 0, read: true, write: true},
			{Name: "SEL_EMP_LANE4", length: 6, shift: 7, defaultvalue: 0, read: true, write: true}},
		0x7B: {{Name: "MODE_IMPROVE_HD3", length: 1, shift: 0, defaultvalue: 0, read: true, write: true}},
	}}

}

func (adcregmap *ADCRegisterMap) GenerateMiscPage() {
	adcregmap.MiscPage = ADCRegisterPage{SPIRegisters: map[uint8][]ADCRegister{
		0x21: {{Name: "VREF_FILT_EN", length: 1, shift: 12, defaultvalue: 0, read: true, write: true}},
		0x2B: {{Name: "PDN_INT_REF", length: 1, shift: 6, defaultvalue: 0, read: true, write: true}},
		0x2C: {
			{Name: "IGNORE_SYSREF", length: 1, shift: 9, defaultvalue: 0, read: true, write: true},
			{Name: "SYSREF_ONE_SHOT_EN", length: 1, shift: 10, defaultvalue: 0, read: true, write: true},
		},
		0x2D: {{Name: "VCM_COARSE_PROG", length: 1, shift: 14, defaultvalue: 0, read: true, write: true}},
		0x2E: {{Name: "VCM_FINE_PROG", length: 3, shift: 0, defaultvalue: 0, read: true, write: true}},
		0x33: {{Name: "EN_VCM_FINE_PROG", length: 1, shift: 0, defaultvalue: 0, read: true, write: true}},
	}}
}

func (adcreg *ADCRegisterMap) SetRegister(name string, value uint16) (string, uint16, uint16) {
	//Set The value of the logical register in the map, once it found it
	page, regaddr, _ := adcreg.GetRegister(name)
	switch page {
	case "MASTER":
		for key, val := range adcreg.MasterPage.SPIRegisters[uint8(regaddr)] {
			if val.Name == name {
				adcreg.MasterPage.SPIRegisters[uint8(regaddr)][key].Value = value
				return page, uint16(regaddr), FormRegister(adcreg.MasterPage.SPIRegisters[uint8(regaddr)])
			}
		}
	case "MISC":
		for key, val := range adcreg.MiscPage.SPIRegisters[uint8(regaddr)] {
			if val.Name == name {
				adcreg.MiscPage.SPIRegisters[uint8(regaddr)][key].Value = value
				return page, uint16(regaddr), FormRegister(adcreg.MiscPage.SPIRegisters[uint8(regaddr)])
			}
		}
	case "DIG":
		for key, val := range adcreg.DigitalChannelPage.SPIRegisters[uint8(regaddr)] {
			if val.Name == name {
				adcreg.DigitalChannelPage.SPIRegisters[uint8(regaddr)][key].Value = value
				return page, uint16(regaddr), FormRegister(adcreg.DigitalChannelPage.SPIRegisters[uint8(regaddr)])
			}
		}
	case "ADC":
		for key, val := range adcreg.ADCPage.SPIRegisters[uint8(regaddr)] {
			if val.Name == name {
				adcreg.ADCPage.SPIRegisters[uint8(regaddr)][key].Value = value
				return page, uint16(regaddr), FormRegister(adcreg.ADCPage.SPIRegisters[uint8(regaddr)])
			}
		}
	case "8CH":
		for key, val := range adcreg.CH8DigitalPage.SPIRegisters[uint8(regaddr)] {
			if val.Name == name {
				adcreg.CH8DigitalPage.SPIRegisters[uint8(regaddr)][key].Value = value
				return page, uint16(regaddr), FormRegister(adcreg.CH8DigitalPage.SPIRegisters[uint8(regaddr)])
			}
		}
	default:
		log.Fatal("non-existing page")
		return "", 0, 0
	}
	return "", 0, 0
}

func (adcreg *ADCRegisterMap) GetRegister(name string) (page string, regaddr uint8, value uint16) {
	// Return the Page where the register is, the address of the SPI register and
	// the value of the SPI register, which includes the selected register
	for key, v := range adcreg.MasterPage.SPIRegisters {
		for _, regv := range v {
			if regv.Name == name {
				return "MASTER", key, FormRegister(v)
			}
		}
	}
	for key, v := range adcreg.ADCPage.SPIRegisters {
		for _, regv := range v {
			if regv.Name == name {
				return "ADC", key, FormRegister(v)
			}
		}
	}
	for key, v := range adcreg.MiscPage.SPIRegisters {
		for _, regv := range v {
			if regv.Name == name {
				return "MISC", key, FormRegister(v)
			}
		}
	}
	for key, v := range adcreg.CH8DigitalPage.SPIRegisters {
		for _, regv := range v {
			if regv.Name == name {
				return "8CH", key, FormRegister(v)
			}
		}
	}
	for key, v := range adcreg.DigitalChannelPage.SPIRegisters {
		for _, regv := range v {
			if regv.Name == name {
				return "DIG", key, FormRegister(v)
			}
		}
	}
	return "NONE", 0, 0
}

func (ctrl *ADCController) Configure() {
	var err error

	//page selection to ADC
	data := []byte{0x11, 0xFF}
	_, err = ctrl.Spictrl.Transfer("ADC", data)
	for key, reg := range ctrl.Registers.ADCPage.SPIRegisters {
		if err != nil {
			log.Fatal(err)
		}
		value := FormRegister(reg)
		data = []byte{key, byte(value>>8) & 0xFF, byte(value & 0xFF)}
		_, err = ctrl.Spictrl.Transfer("ADC", data)

	}
	//page selection to DIG
	data = []byte{0x13, 0xFF}
	_, err = ctrl.Spictrl.Transfer("ADC", data)
	for key, reg := range ctrl.Registers.DigitalChannelPage.SPIRegisters {
		if err != nil {
			log.Fatal(err)
		}
		value := FormRegister(reg)
		data = []byte{key, byte(value>>8) & 0xFF, byte(value & 0xFF)}
		_, err = ctrl.Spictrl.Transfer("ADC", data)
	}
	//page selection to CH8
	data = []byte{0x12, 0b10}
	_, err = ctrl.Spictrl.Transfer("ADC", data)
	for key, reg := range ctrl.Registers.CH8DigitalPage.SPIRegisters {
		if err != nil {
			log.Fatal(err)
		}
		value := FormRegister(reg)
		data = []byte{key, byte(value>>8) & 0xFF, byte(value & 0xFF)}
		_, err = ctrl.Spictrl.Transfer("ADC", data)
	}

	//page selection to Misc
	data = []byte{0x12, 0b1}
	_, err = ctrl.Spictrl.Transfer("ADC", data)
	for key, reg := range ctrl.Registers.MiscPage.SPIRegisters {
		if err != nil {
			log.Fatal(err)
		}
		value := FormRegister(reg)
		data = []byte{key, byte(value>>8) & 0xFF, byte(value & 0xFF)}
		_, err = ctrl.Spictrl.Transfer("ADC", data)
	}
}

func (ctrl *ADCController) WriteMemoryConfigurationToYAML(filename string) {
	data, err := yaml.Marshal(&ctrl.Registers)
	if err != nil {
		log.Fatal(err)
	}

	err2 := ioutil.WriteFile(filename, data, 0x777)

	if err2 != nil {

		log.Fatal(err2)
	}

	fmt.Println("data written to ", filename)
}

func (ctrl *ADCController) ReadConfigurationYAMLtoMemory(filename string) {
	log.Println("Reading configuration from file ", filename)
	yfile, err := ioutil.ReadFile(filename)
	if err != nil {

		log.Fatal(err)
	}

	err2 := yaml.Unmarshal(yfile, &ctrl.Registers)

	if err2 != nil {
		log.Fatal(err2)
	}
}
