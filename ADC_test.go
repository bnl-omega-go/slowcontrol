//go:build linux || darwin
// +build linux darwin

package SlowControl

import "testing"

func TestReadADCFile(t *testing.T) {
	type args struct {
		filename string
	}
	tests := []struct {
		name string
		args args
	}{
		{name: "test", args: args{filename: "example.cfg"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ReadADCFile(tt.args.filename)
		})
	}
}
