package SlowControl

import (
	"time"
)

func PreciserSleep(nanoseconds int) {
	time.Sleep(time.Duration(nanoseconds) * time.Nanosecond)

}
