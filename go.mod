module gitlab.cern.ch/bnl-omega-go/slowcontrol

go 1.17

//replace  gitlab.cern.ch/bnl-omega-go/axi => ../AXIController

require (
	gitlab.cern.ch/bnl-omega-go/axi v0.0.0-20220706170021-926f1f233eb6
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
	periph.io/x/conn/v3 v3.6.10
	periph.io/x/host/v3 v3.7.2
)

require (
	github.com/alecthomas/unsafeslice v0.1.0 // indirect
	github.com/codehardt/mmap-go v1.0.1 // indirect
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
)
