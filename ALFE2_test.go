package SlowControl

import (
	"reflect"
	"testing"
)

func TestGenerateI2CRegisters(t *testing.T) {
	tests := []struct {
		name string
		want [][]uint32
	}{
		{"test", [][]uint32{{0, 255}, {1, 23}, {2, 255}, {3, 0}, {4, 224}, {5, 199}, {6, 7}, {7, 254}, {8, 7}, {9, 73}, {10, 228}, {11, 127}, {12, 0}, {13, 195}, {14, 63}, {15, 7}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, r, _, _ := GetALFE2RegisterMaps()
			if got := GenerateI2CRegisters(r); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GenerateI2CRegisters() = %v, want %v", got, tt.want)
			}
		})
	}
}
