//go:build linux || darwin
// +build linux darwin

package SlowControl

import (
	"fmt"
	"io/ioutil"
	"log"
	"sort"
	"strconv"

	AXI "gitlab.cern.ch/bnl-omega-go/axi"
	yaml "gopkg.in/yaml.v3"
)

func bitfield(n, l uint64) []int {
	//n in integer to convert to bitlist, l is the fixed length of the list
	format := fmt.Sprintf("%%0%vb", l)
	b := fmt.Sprintf(format, n)
	result := make([]int, l)
	for i, val := range b {
		result[i], _ = strconv.Atoi(string(val))
	}
	return result
}

func bitlisttoInt(bitlist []int) int64 {
	// transform a bitlist to a int
	out := int64(0)
	//fmt.Println("bitlist ", bitlist)
	for _, bit := range bitlist {
		out = (out << 1) | int64(bit)
	}
	return out & 0xFF
}

func reverse(numbers []int) []int {
	n := len(numbers)
	newnumbers := make([]int, n)
	for i, number := range numbers {
		newnumbers[n-1-i] = number
	}
	return newnumbers
}

func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}

type alfe2controller interface {
	SetRegister(name string, value uint32)
	//GetRegister(name string) uint32
	Configure()
	ReadConfiguration() []map[string][]int
}

type ALFE2Controller struct {
	alfe2controller
	axiic                  *AXIIC
	alfve2_registers_50Ohm map[string]*RegDescription
	alfve2_registers_25Ohm map[string]*RegDescription
	alfve2_registers       map[string]*RegDescription
	i2cmap                 [][]map[string][]int
	Addr                   int
	DevName                string
	delay                  int
	Debug                  bool
}

func NewALFE2Controller(name string, addr int, base int64, delay int, dryrun bool) *ALFE2Controller {
	i2cmap, reg, reg50, reg25 := GetALFE2RegisterMaps()
	ctrl := ALFE2Controller{DevName: name, Addr: addr, i2cmap: i2cmap,
		alfve2_registers: reg, alfve2_registers_50Ohm: reg50, alfve2_registers_25Ohm: reg25}
	if !dryrun {
		ctrl.axiic = NewAXIIC(base, delay)
	}
	return &ctrl
}

func ConnectALFE2Controller(axictrl *AXI.AXIController, name string, addr int, base int64, delay int, dryrun bool) *ALFE2Controller {
	i2cmap, reg, reg50, reg25 := GetALFE2RegisterMaps()
	ctrl := ALFE2Controller{DevName: name, Addr: addr, i2cmap: i2cmap,
		alfve2_registers: reg, alfve2_registers_50Ohm: reg50, alfve2_registers_25Ohm: reg25}
	if !dryrun {
		ctrl.axiic = NewConnectedIIC(axictrl, base, delay)
	}
	return &ctrl
}

func (c *ALFE2Controller) ResetI2C() {
	c.axiic.Reset()
}

func (c *ALFE2Controller) SetClockDivider(div uint32) {
	c.axiic.SetClockDivider(div)
}

func (c *ALFE2Controller) SetRegister(regname string, value uint32) {
	(c.alfve2_registers[regname]).SetValue(uint64(value))
	c.Configure(false, false)
}

func (c *ALFE2Controller) Configure(verbose, dryrun bool) (written []uint32, errors []error) {

	transactions := GenerateI2CRegisters(c.alfve2_registers)

	written = make([]uint32, 16)
	errs := make([]error, 3)
	for i, t := range transactions {
		if verbose {
			log.Printf("Writing %x to register with address %x \n", t[1], t[0])
		}
		if !dryrun {
			errs[0] = c.axiic.Write(uint32(c.Addr+0), t[0]&0xFF)
			errs[1] = c.axiic.Write(uint32(c.Addr+1), 0)
			errs[2] = c.axiic.Write(uint32(c.Addr+2), t[1])
		}
		written[i] = t[1]

		for _, e := range errs {
			if e != nil {
				errors = append(errors, e)
			}
		}
	}
	return
}

func (c *ALFE2Controller) ConfigureFast(verbose, dryrun bool) (written []uint32, errors []error) {

	transactions := GenerateI2CRegisters(c.alfve2_registers)
	written = make([]uint32, 16)

	err1 := c.axiic.Write(uint32(c.Addr+0), 0)
	err2 := c.axiic.Write(uint32(c.Addr+1), 0)
	if err1 != nil {
		errors = append(errors, err1)
	}
	if err2 != nil {
		errors = append(errors, err2)
	}

	for i, t := range transactions {
		if verbose {
			log.Printf("Writing %x to register with address %x \n", t[1], t[0])
		}
		if !dryrun {
			err := c.axiic.Write(uint32(c.Addr+3), t[1])
			if err != nil {
				errors = append(errors, err)
			}
		}
		written[i] = t[1]
	}
	return
}

func (c *ALFE2Controller) Readback(verbose, dryrun bool) (readback []uint32, errors []error) {

	transactions := GenerateI2CRegisters(c.alfve2_registers)
	ok := true
	errs := make([]error, 3)
	readback = make([]uint32, 16)
	for i, t := range transactions {
		if !dryrun {
			errs[0] = c.axiic.Write(uint32(c.Addr+0), t[0]&0xFF)
			errs[1] = c.axiic.Write(uint32(c.Addr+1), 0)
			var value uint32
			value, errs[2] = c.axiic.Read(uint32(c.Addr + 2))

			if value != t[1] {
				ok = false
			}

			if verbose {
				log.Printf("Reading %x from register with address %x \n", value, t[0])
			}

			for _, e := range errs {
				if e != nil {
					errors = append(errors, e)
				}
			}
			readback[i] = value
		}
	}
	if !ok {
		log.Println("ERROR: Readback not not match written data!")
		errors = append(errors, fmt.Errorf("ERROR: Readback not not match written data! expt: %v, rb:%v", transactions, readback))
	}
	return
}

func (c *ALFE2Controller) ReadbackFast(verbose, dryrun bool) (readback []uint32, errors []error) {

	transactions := GenerateI2CRegisters(c.alfve2_registers)
	readback = make([]uint32, 16)
	ok := true

	err1 := c.axiic.Write(uint32(c.Addr+0), 0)
	err2 := c.axiic.Write(uint32(c.Addr+1), 0)
	if err1 != nil {
		errors = append(errors, err1)
	}
	if err2 != nil {
		errors = append(errors, err2)
	}

	for i, t := range transactions {
		if !dryrun {

			value, err := c.axiic.Read(uint32(c.Addr + 3))
			if value != t[1] {
				ok = false
			}

			if err != nil {
				errors = append(errors, err)
			}

			if verbose {
				log.Printf("Reading %x from register with address %x \n", value, t[0])
			}
			readback[i] = value

		}
	}
	if !ok {
		log.Println("ERROR: Readback not not match written data!")
		errors = append(errors, fmt.Errorf("ERROR: Readback not not match written data! expt: %v,  rb:%v", transactions, readback))
	}
	return
}

func (ctrl *ALFE2Controller) WriteRegister(address, value uint32) []error {
	errors := make([]error, 3)
	errors[0] = ctrl.axiic.Write(uint32(ctrl.Addr+0), address)
	errors[1] = ctrl.axiic.Write(uint32(ctrl.Addr+1), 0)
	errors[2] = ctrl.axiic.Write(uint32(ctrl.Addr+2), value)
	return errors
}

func (ctrl *ALFE2Controller) ReadRegister(address uint32) (uint32, []error) {
	errors := make([]error, 3)

	errors[0] = ctrl.axiic.Write(uint32(ctrl.Addr+0), address)
	errors[1] = ctrl.axiic.Write(uint32(ctrl.Addr+1), 0)
	var value uint32
	value, errors[2] = ctrl.axiic.Read(uint32(ctrl.Addr + 2))
	return value, errors

}

func (ctrl *ALFE2Controller) WriteConfiguration(filename string) {
	data, err := yaml.Marshal(&ctrl.alfve2_registers)
	if err != nil {
		log.Fatal(err)
	}

	err2 := ioutil.WriteFile(filename, data, 777)

	if err2 != nil {

		log.Fatal(err2)
	}

	fmt.Println("ALFE2 Registers written to ", filename)
	// for key, reg := range ctrl.alfve2_registers {
	// 	log.Printf("%v = %x \n", key, (*reg).Value)
	// }
}

func (ctrl *ALFE2Controller) ReadConfiguration(filename string, verbose bool) {
	log.Println("Reading configuration from file ", filename)
	yfile, err := ioutil.ReadFile(filename)
	if err != nil {

		log.Fatal(err)
	}

	err2 := yaml.Unmarshal(yfile, &ctrl.alfve2_registers)

	if err2 != nil {
		log.Fatal(err2)
	}
	log.Printf("ALFE2 Registers read from %s\n", filename)

	if verbose {
		for key, reg := range ctrl.alfve2_registers {
			log.Printf("%v = %x \n", key, (*reg).Value)
		}
	}
}

func (c *ALFE2Controller) IncrementConfiguration() {
	safedacs := []string{"PA_OUT_OFFSET", "IMPED_COARSE", "IMPED_FINE", "PA_OUTPUT_DC", "SH_PT", "EN_SH_LG_GAIN_BOOST", "SH_OUTPUT_DC", "SH_SUM_GAIN_CH0", "SH_SUM_GAIN_CH1", "SH_SUM_GAIN_CH2", "SH_SUM_GAIN_CH3", "SH_SUM_OUTPUT", "UNUSED"}
	for key := range c.alfve2_registers {
		if contains(safedacs, key) {
			if c.alfve2_registers[key].Value == ((1 << c.alfve2_registers[key].Length) - 1) {
				c.alfve2_registers[key].Value = 0
			} else {
				c.alfve2_registers[key].Value = ((c.alfve2_registers[key].Value << 1) & 0xFFFFFFE) + 1
			}
		}
		log.Printf("incrementing %v to %v", key, c.alfve2_registers[key].Value)
	}
}

func (ctrl *ALFE2Controller) SetRegisterValue(name string, value uint64) (errors []error) {

	if ctrl.alfve2_registers[name] != nil {
		if value>>ctrl.alfve2_registers[name].Length != 0 {
			log.Print("Value provided longer than register, please check value provided")
			return
		}
		(*ctrl.alfve2_registers[name]).SetValue(value)
	}
	// to_reprogram := make([]int, 0)
	// for addr, i2c_reg := range ctrl.i2cmap {
	// 	if i2c_reg[name] != nil {
	// 		to_reprogram = append(to_reprogram, addr)
	// 	}
	// }

	to_reprogram := make([][]int, 0)
	for addr, i2c_reg := range ctrl.i2cmap {
		for i, reg := range i2c_reg {
			if reg[name] != nil {
				to_reprogram = append(to_reprogram, []int{addr, i})
			}
		}
	}

	trs := GenerateI2CRegisters(ctrl.alfve2_registers)

	for _, trp := range to_reprogram {
		addr := trp[0]
		//log.Printf("Writing register %x with value %x", uint32(addr), trs[addr][1])
		errors = append(errors, ctrl.WriteRegister(uint32(addr), trs[addr][1])...)
		value, rderrs := ctrl.ReadRegister(uint32(addr))
		errors = append(errors, rderrs...)
		if value != trs[addr][1] {
			log.Printf("Error in readback of register %x : %x!=%x", addr, value, trs[addr][1])
			errors = append(errors, fmt.Errorf("error in readback of register %x : %x!=%x", addr, value, trs[addr][1]))
		}
	}
	return
}

func (ctrl *ALFE2Controller) GetRegisterValue(name string) (register_value uint32, errors []error) {
	// if ctrl.alfve2_registers[name] != nil {
	// 	(*ctrl.alfve2_registers[name]).SetValue(value)
	// }

	to_reprogram := make([][]int, 0)
	for addr, i2c_reg := range ctrl.i2cmap {
		for i, reg := range i2c_reg {
			if reg[name] != nil {
				to_reprogram = append(to_reprogram, []int{addr, i})
			}
		}
	}

	//trs := GenerateI2CRegisters(ctrl.alfve2_registers)

	for _, trp := range to_reprogram {
		//log.Printf("Writing register %x with value %x", uint32(addr), trs[addr][1])
		//ctrl.WriteRegister(uint32(addr), trs[addr][1])
		value, errs := ctrl.ReadRegister(uint32(trp[0]))
		errors = append(errors, errs...)
		register_value += ((value >> uint32(ctrl.i2cmap[trp[0]][trp[1]][name][2])) & (uint32(Thermometer(ctrl.i2cmap[trp[0]][trp[1]][name][3])))) << uint32(ctrl.i2cmap[trp[0]][trp[1]][name][0])
	}
	return
}

func (ctrl *ALFE2Controller) I2CWrite(address, value uint32) error {
	return ctrl.axiic.Write(uint32(address), value)
}

func (ctrl *ALFE2Controller) Set50OhmDefaults() {
	ctrl.alfve2_registers = ctrl.alfve2_registers_50Ohm
}

func (ctrl *ALFE2Controller) Set25OhmDefaults() {
	ctrl.alfve2_registers = ctrl.alfve2_registers_25Ohm
}

type RegDescription struct {
	Length uint64
	Value  uint64
}

func (reg *RegDescription) SetValue(value uint64) {
	reg.Value = value
}

func GetALFE2RegisterMaps() (alfe2_i2c_map [][]map[string][]int, alfve2_registers_50Ohm, alfve2_registers_25Ohm, alfve2_registers map[string]*RegDescription) {

	alfe2_i2c_map = [][]map[string][]int{
		0x0: {{"CH_PWR": {0, 4, 0, 4}}, {"PA_FB_R0": {0, 2, 4, 2}}, {"PA_FB_C0": {0, 2, 6, 2}}},
		0x1: {{"PA_OUT_OFFSET": {0, 4, 0, 4}}, {"PA_FB_C1": {0, 2, 4, 2}}, {"IMPED_COARSE": {0, 2, 6, 2}}},
		0x2: {{"IMPED_FINE": {0, 8, 0, 8}}},
		0x3: {{"IMPED_FINE": {8, 16, 0, 8}}},
		0x4: {{"IMPED_FINE": {16, 21, 0, 5}}, {"PA_FB_R1": {0, 1, 5, 1}}, {"PA_OUTPUT_DC": {0, 2, 6, 2}}},
		0x5: {{"PA_OUTPUT_DC": {2, 8, 0, 6}}, {"SH_PT": {0, 2, 6, 2}}},
		0x6: {{"SH_PT": {2, 10, 0, 8}}},
		0x7: {{"ENABLE_50_SH": {0, 1, 0, 1}}, {"SH_OUTPUT_DC": {0, 7, 1, 7}}},
		0x8: {{"SH_OUTPUT_DC": {7, 15, 0, 8}}},
		0x9: {{"SH_SUM_PWR": {0, 1, 0, 1}}, {"SH_SUM_GAIN_CH0": {0, 3, 1, 3}}, {"SH_SUM_GAIN_CH1": {0, 3, 4, 3}}, {"SH_SUM_GAIN_CH2": {0, 1, 7, 1}}},
		0xA: {{"SH_SUM_GAIN_CH2": {1, 3, 0, 2}}, {"SH_SUM_GAIN_CH3": {0, 3, 2, 3}}, {"SH_SUM_OUTPUT": {0, 3, 5, 3}}},
		0xB: {{"SH_SUM_OUTPUT": {3, 11, 0, 8}}},
		0xC: {{"SH_SUM_OUTPUT": {11, 15, 0, 4}}, {"DACN0_CUR": {0, 4, 4, 4}}},
		0xD: {{"DACN0_CUR": {4, 6, 0, 2}}, {"DACN1_CUR": {0, 6, 2, 6}}},
		0xE: {{"DACP1_CUR": {0, 6, 0, 6}}, {"BGR_SEL": {0, 2, 6, 2}}},
		0xF: {{"PA12V_CUR": {0, 1, 0, 1}}, {"SH1V2_CUR": {0, 1, 1, 1}}, {"PA2V5_CUR": {0, 1, 2, 1}}, {"UNUSED": {0, 4, 4, 4}}},
	}

	alfve2_registers_50Ohm = map[string]*RegDescription{
		"CH_PWR":          {4, 0b0000},                   // migh be 1111    // Channel power
		"PA_FB_R0":        {2, 0b01},                     // Feedback Resistor 0
		"PA_FB_C0":        {2, 0b01},                     // feedback capacitor 0
		"PA_OUT_OFFSET":   {4, 0b0001},                   // Preamp Output Offset
		"PA_FB_C1":        {2, 0b01},                     // Preamp Feedback Capacitor 1
		"IMPED_COARSE":    {2, 0b11},                     // Coarse impedance adjustment
		"IMPED_FINE":      {21, 0b000000000011111111111}, // Fine impedance adjustment
		"PA_FB_R1":        {1, 0b1},                      // Preamp Feedback Resistor 1
		"PA_OUTPUT_DC":    {8, 0b00011111},               // Preamp DC output level
		"SH_PT":           {10, 0b0000001111},            // Shaper Peaking Time
		"ENABLE_50_SH":    {1, 0b0},                      // not sure 0 or 1 for 50 OHM    // Enable 50 Ohm Shaping
		"SH_OUTPUT_DC":    {15, 0b000001111111111},       // Shaper DC output level
		"SH_SUM_PWR":      {1, 0b0},                      // Sum circuit shaper Power
		"SH_SUM_GAIN_CH0": {3, 0b001},                    // Sum circuit gain for CH0
		"SH_SUM_GAIN_CH1": {3, 0b001},                    // Sum circuit gain for CH1
		"SH_SUM_GAIN_CH2": {3, 0b001},                    // Sum circuit gain for CH2
		"SH_SUM_GAIN_CH3": {3, 0b001},                    // Sum circuit gain for CH3
		"SH_SUM_OUTPUT":   {15, 0b000001111111111},       // Sum circuit Output level
		"DACN0_CUR":       {6, 0b110000},                 // DACN0 current
		"DACN1_CUR":       {6, 0b110000},                 // DACN1 current
		"DACP1_CUR":       {6, 0b001111},                 // DACP1 current
		"BGR_SEL":         {2, 0b01},                     // maybe 01    // Bandgap source selection, 11 for external, 01 for internal
		"PA12V_CUR":       {1, 0b1},                      // Preamp 1,2V current
		"SH1V2_CUR":       {1, 0b1},                      // Shaper 1,2V current
		"PA2V5_CUR":       {1, 0b1},                      // Preamp 2.5V current
		"UNUSED":          {5, 0b00000},                  // unused bits
	}

	alfve2_registers_25Ohm = map[string]*RegDescription{
		"CH_PWR":          {4, 0b0000},                   // migh be 1111    // Channel power
		"PA_FB_R0":        {2, 0b11},                     // Feedback Resistor 0
		"PA_FB_C0":        {2, 0b11},                     // feedback capacitor 0
		"PA_OUT_OFFSET":   {4, 0b1111},                   // Preamp Output Offset
		"PA_FB_C1":        {2, 0b11},                     // Preamp Feedback Capacitor 1
		"IMPED_COARSE":    {2, 0b00},                     // Coarse impedance adjustment
		"IMPED_FINE":      {21, 0b000000000111111111111}, // Fine impedance adjustment
		"PA_FB_R1":        {1, 0b0},                      // Preamp Feedback Resistor 1
		"PA_OUTPUT_DC":    {8, 0b00011111},               // Preamp DC output level
		"SH_PT":           {10, 0b0000000111},            // Shaper Peaking Time
		"ENABLE_50_SH":    {1, 0b0},                      // not sure 0 or 1 for 50 OHM    // Enable 50 Ohm Shaping
		"SH_OUTPUT_DC":    {15, 0b000001111111111},       // Shaper DC output level
		"SH_SUM_PWR":      {1, 0b0},                      // Sum circuit shaper Power
		"SH_SUM_GAIN_CH0": {3, 0b001},                    // Sum circuit gain for CH0
		"SH_SUM_GAIN_CH1": {3, 0b001},                    // Sum circuit gain for CH1
		"SH_SUM_GAIN_CH2": {3, 0b001},                    // Sum circuit gain for CH2
		"SH_SUM_GAIN_CH3": {3, 0b001},                    // Sum circuit gain for CH3
		"SH_SUM_OUTPUT":   {15, 0b000001111111111},       // Sum circuit Output level
		"DACN0_CUR":       {6, 0b110000},                 // DACN0 current
		"DACN1_CUR":       {6, 0b110000},                 // DACN1 current
		"DACP1_CUR":       {6, 0b001111},                 // DACP1 current
		"BGR_SEL":         {2, 0b01},                     // maybe 01    // Bandgap source selection, 11 for external, 01 for internal
		"PA12V_CUR":       {1, 0b1},                      // Preamp 1,2V current
		"SH1V2_CUR":       {1, 0b1},                      // Shaper 1,2V current
		"PA2V5_CUR":       {1, 0b1},                      // Preamp 2.5V current
		"UNUSED":          {5, 0b00000},                  // unused bits
	}
	alfve2_registers = alfve2_registers_50Ohm
	return
}

func GenerateI2CRegisters(alfve2_registers map[string]*RegDescription) [][]uint32 {
	alfe2_i2c_map, _, _, _ := GetALFE2RegisterMaps()
	//Bitbanging to pack the logical register in a list of I2C registers
	transactions := make([][]uint32, 0)
	// for each I2C address
	orderedi2ckeys := make([]int, 0)
	for k := range alfe2_i2c_map {
		orderedi2ckeys = append(orderedi2ckeys, k)
	}
	sort.Ints(orderedi2ckeys)

	for _, key := range orderedi2ckeys {
		value := alfe2_i2c_map[key]

		type Keys struct {
			key   string
			index int
		}

		orderedalfekeys := make([]Keys, 0)
		for i, v := range value {
			for k := range v {
				orderedalfekeys = append(orderedalfekeys, Keys{k, i})
			}
		}
		//sort.Strings(orderedalfekeys)
		reg_value_bits := make([]int, 0)
		//check the content of this register from mapping,
		//then fetch the required bit from logical registers (in mapping) and assemble the value
		//fmt.Println("Register ", key, " content ", value)
		for _, reg := range orderedalfekeys {
			rangevalue := value[reg.index][reg.key]
			//fmt.Printf("Processing  %v with value %v \n", reg, alfve2_registers[reg.key].Value)
			value_bits := bitfield(alfve2_registers[reg.key].Value, alfve2_registers[reg.key].Length)
			value_bits = reverse(value_bits)
			value_bits = value_bits[rangevalue[0]:rangevalue[1]]
			value_bits = reverse(value_bits)
			reg_value_bits = append(value_bits, reg_value_bits...)
			//fmt.Printf("inserting  in %v  %v %v \n", reg, " from ", value_bits)
			//fmt.Println(reg_value_bits)
		}
		transactions = append(transactions, []uint32{uint32(key), uint32(bitlisttoInt(reg_value_bits))})
	}
	//return list of address and value to write
	return transactions
}
